﻿using System;
using System.Collections.Generic;

namespace Architecture.UnityModule.GlobalEvents
{
	public struct PublishRecord
	{
		public object Publisher;
		public List<object> Subscribers;
		public string EventToString;
		public DateTime PublishTime;

		public PublishRecord(object publisher, List<object> subscribers, string eventToString) : this()
		{
			Publisher = publisher;
			Subscribers = subscribers;
			EventToString = eventToString;
			PublishTime = DateTime.Now;
		}
	}
}