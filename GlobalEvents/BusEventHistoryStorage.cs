﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Architecture.UnityModule.GlobalEvents
{
	public static class BusEventHistoryStorage
	{
		public static Dictionary<Type, List<PublishRecord>> EventToPublishRecords =
			new Dictionary<Type, List<PublishRecord>>();

		public static bool IsEnable = false;
		public static Stopwatch sw = new Stopwatch();

		public static void AddPublishRecord(IGlobalEvent @event, object publisher, List<object> subscribers)
		{
			var eventType = @event.GetType();
			if (!EventToPublishRecords.ContainsKey(eventType))
			{
				EventToPublishRecords.Add(eventType, new List<PublishRecord>());
			}

			EventToPublishRecords[eventType].Add(new PublishRecord(publisher, subscribers, @event.ToString()));
		}
	}
}