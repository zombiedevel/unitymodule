﻿using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;

namespace Architecture.UnityModule.GlobalEvents
{
	public static class BusEventManager<T> where T : IGlobalEvent
	{
		private static readonly List<SubscribeInfo<T>> _subscribtionInfos = new List<SubscribeInfo<T>>();

		public static void Publish(object publisher, T data)
		{
			BusEventHistoryStorage.sw.Start();
			for (var i = 0; i < _subscribtionInfos.Count; i++)
			{
				_subscribtionInfos[i].HandleAction?.Invoke(data);
			}
			BusEventHistoryStorage.sw.Stop();
//			Debug.Log(BusEventHistoryStorage.sw.Elapsed.TotalMilliseconds + " " + Time.timeSinceLevelLoad + " "  + (BusEventHistoryStorage.sw.Elapsed.TotalMilliseconds / 1000 / Time.timeSinceLevelLoad * 100) + "%" );

			if (BusEventHistoryStorage.IsEnable)
			{
				var subscribers = new List<object>();
				foreach (var subscribeInfo in _subscribtionInfos)
				{
					subscribers.Add(subscribeInfo.Subscriber);
				}
				BusEventHistoryStorage.AddPublishRecord(data, publisher, subscribers);
			}
		}

		public static void Subscribe(object subscriber, Action<T> action, int order = 0)
		{
			for (var i = 0; i < _subscribtionInfos.Count; i++)
			{
				if (_subscribtionInfos[i].Subscriber == subscriber)
				{
					Debug.LogError("Subscriber already exist");
					return;
				}
			}

			var item = new SubscribeInfo<T>
			{
				HandleAction = action,
				Priority = order,
				Subscriber = subscriber
			};
			_subscribtionInfos.Add(item);
			_subscribtionInfos.Sort();
		}

		public static void Unsubscribe(object subscriber)
		{
			for (var i = 0; i < _subscribtionInfos.Count; i++)
			{
				if (_subscribtionInfos[i].Subscriber == subscriber)
				{
					_subscribtionInfos.RemoveAt(i);
					return;
				}
			}

			Debug.LogError("Subscriber not exist");
		}

		public static void RemoveAllSubscribers()
		{
			_subscribtionInfos.Clear();
		}
	}
}