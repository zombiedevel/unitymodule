﻿using System;

namespace Architecture.UnityModule.GlobalEvents
{
	public class SubscribeInfo<T> : IComparable where T : IGlobalEvent
	{
		public object Subscriber;
		public Action<T> HandleAction;
		public int Priority;

		public int CompareTo(object obj)
		{
			var other = (SubscribeInfo<T>) obj;
			return -Priority.CompareTo(other.Priority);
		}
	}
}