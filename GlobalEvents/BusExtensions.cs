﻿using System;

namespace Architecture.UnityModule.GlobalEvents
{
	public static class BusExtensions
	{
		public static void Subscribe<T>(this object subscriber, Action<T> action, int order = 0) where T : IGlobalEvent
		{
			BusEventManager<T>.Subscribe(subscriber, action, order);
		}

		public static void Unsubscribe<T>(this object subscriber) where T : IGlobalEvent
		{
			BusEventManager<T>.Unsubscribe(subscriber);
		}

		public static void Publish<T>(this object publisher, T data) where T : IGlobalEvent
		{
			BusEventManager<T>.Publish(publisher, data);
		}
	}
}