﻿using System;
using System.Text;
using Architecture.UnityModule.GlobalEvents;
using UnityEditor;
using UnityEngine;

namespace Architecture.Core.GlobalEvents.Editor
{
	public class GlobalEventsVisualizer : EditorWindow
	{
		[MenuItem("GlobalEvents/Visualizer")]
		private static void DoIt()
		{
			GetWindow(typeof(GlobalEventsVisualizer));
		}

		private Type _activeType;
		private bool _drawSubscribers = false;

		private void OnInspectorUpdate()
		{
			Repaint();
		}

		public void OnGUI()
		{
			BusEventHistoryStorage.IsEnable = true;

			if (!EditorApplication.isPlaying)
			{
				GUILayout.Label("Необходимо запустить игру");
				return;
			}

			GUILayout.Label(BusEventHistoryStorage.EventToPublishRecords.Count.ToString());

			if (GUILayout.Button("DrawSubscribers: " + _drawSubscribers))
			{
				_drawSubscribers = !_drawSubscribers;
			}

			foreach (var key in BusEventHistoryStorage.EventToPublishRecords.Keys)
			{
				var selected = key == _activeType ? " === " : "";
				if (GUILayout.Button(
					$"{selected}\t{key.Name}\t{BusEventHistoryStorage.EventToPublishRecords[key].Count}\t{selected}"))
				{
					_activeType = key;
				}
			}

			if (_activeType == null || !BusEventHistoryStorage.EventToPublishRecords.ContainsKey(_activeType))
			{
				return;
			}

			var sb = new StringBuilder();
			var count = BusEventHistoryStorage.EventToPublishRecords[_activeType].Count;
			for (var i = 0; i < Mathf.Min(20, count); i++)
			{
				var value = BusEventHistoryStorage.EventToPublishRecords[_activeType][count - i - 1];
				sb.Append(
					$"{value.PublishTime.ToLongTimeString()}\t{value.Publisher.GetType().Name}\t=>\t{value.EventToString}\t=>\t{value.Subscribers.Count}\n");

				if (_drawSubscribers)
				{
					foreach (var valueSubscriber in value.Subscribers)
					{
						sb.Append("\t\t\t" + valueSubscriber.GetType().Name + "\n");
					}
				}
				sb.Append("\n");
			}
			GUILayout.TextArea(sb.ToString());

			Repaint();
		}
	}
}