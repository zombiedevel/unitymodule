﻿using Architecture.Implementation.Localization;
using UnityEngine;
using UnityEngine.UI;

namespace Architecture.UnityModule.Localization
{
	public class LangBnd : MonoBehaviour
	{
		public enum WordType
		{
			Normal = 0,
			ToUpper = 1
		}

		public string LangKey;
		public WordType Type = WordType.Normal;

		private void Start()
		{
			var word = LangKey.Localized();
			if (Type == WordType.ToUpper)
			{
				word = word.ToUpperInvariant();
			}

			GetComponent<Text>().text = word;
		}
	}
}