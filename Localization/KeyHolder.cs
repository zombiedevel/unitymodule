﻿using Architecture.Implementation.Localization;

namespace Architecture.UnityModule.Localization
{
	public struct KeyHolder
	{
		public string Key;

		public KeyHolder(string k)
		{
			Key = k;
		}

		public override string ToString()
		{
			return Key.Localized();
		}

		public static implicit operator string(KeyHolder k)
		{
			return k.ToString();
		}

		public string ToUpper()
		{
			return ToString().ToUpperInvariant();
		}

		public string ToLower()
		{
			return ToString().ToUpperInvariant();
		}
	}
}