﻿using System.Globalization;
using Architecture.UnityModule.Serializer;
using UnityEngine;

namespace Architecture.UnityModule.SafeTypes
{
	public struct SafeInt
	{
		[ParserSerialize] private readonly int _rnd;
		[ParserSerialize] private readonly int _hiddenValue;

		private const int _CONSTANT_OFFSET = 103;
		private const int _CONSTANT_DIV = 57;

		private SafeInt(int value)
		{
			_rnd = Random.Range(-10000, +10000);
			_hiddenValue = value + (_rnd % _CONSTANT_DIV) + _CONSTANT_OFFSET;
		}

		private int GetValue ()
		{
			return _hiddenValue - (_rnd % _CONSTANT_DIV) - _CONSTANT_OFFSET;
		}

		public static implicit operator int(SafeInt k)
		{
			return k.GetValue();
		}

		public static implicit operator SafeInt(int k)
		{
			return new SafeInt(k);
		}

		public override string ToString()
		{
			return GetValue().ToString(CultureInfo.InvariantCulture);
		}

		public override bool Equals(object obj)
		{
			if (!(obj is SafeInt))
			{
				return false;
			}

			SafeInt other = (SafeInt) obj;
			return (other.GetValue() == GetValue());
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}