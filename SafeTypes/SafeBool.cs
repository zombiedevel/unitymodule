﻿using System.Globalization;
using Architecture.UnityModule.Extensions;
using Architecture.UnityModule.Serializer;
using UnityEngine;

namespace Architecture.UnityModule.SafeTypes
{
	public struct SafeBool
	{
		[ParserSerialize] private readonly int _hiddenValue;

		private const int _CONSTANT_MUL = 13;
		private static readonly int[] _divIsTrue = {1, 2, 7, 9, 10};
		private static readonly int[] _someDivIsFalse = {0, 3, 5, 8};

		private SafeBool(bool value)
		{
			var mul1 = Random.Range(70, 190);
			_hiddenValue = mul1 * _CONSTANT_MUL;

			if (value)
			{
				_hiddenValue += _divIsTrue.RandomItem();
			}
			else
			{
				_hiddenValue += _someDivIsFalse.RandomItem();
			}
		}

		private bool GetValue()
		{
			int num = _hiddenValue % _CONSTANT_MUL;

			foreach (var i in _divIsTrue)
			{
				if (num == i)
				{
					return true;
				}
			}
			return false;
		}

		public static implicit operator bool(SafeBool k)
		{
			return k.GetValue();
		}

		public static implicit operator SafeBool(bool k)
		{
			return new SafeBool(k);
		}

		public override string ToString()
		{
			return GetValue().ToString(CultureInfo.InvariantCulture);
		}

		public override bool Equals(object obj)
		{
			if (!(obj is SafeBool))
			{
				return false;
			}

			SafeBool other = (SafeBool) obj;
			return (other.GetValue() == GetValue());
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}