﻿using System;
using System.Globalization;
using Architecture.UnityModule.Serializer;
using Random = UnityEngine.Random;

namespace Architecture.UnityModule.SafeTypes
{
	public struct SafeFloat
	{
		[ParserSerialize] private readonly float _rnd;
		[ParserSerialize] private readonly float _hiddenValue;

		private const float _CONSTANT_OFFSET = -47;
		private const float _CONSTANT_DIV = 89;

		private SafeFloat(float value)
		{
			_rnd = Random.Range(-10000, +10000);
			_hiddenValue = value + (_rnd%_CONSTANT_DIV) + _CONSTANT_OFFSET;
		}

		private float GetValue()
		{
			return _hiddenValue - (_rnd%_CONSTANT_DIV) - _CONSTANT_OFFSET;
		}

		public static implicit operator float(SafeFloat k)
		{
			return k.GetValue();
		}

		public static implicit operator SafeFloat(float k)
		{
			return new SafeFloat(k);
		}

		public override string ToString()
		{
			return GetValue().ToString(CultureInfo.InvariantCulture);
		}

		public override bool Equals(object obj)
		{
			if (!(obj is SafeFloat))
			{
				return false;
			}

			SafeFloat other = (SafeFloat) obj;
			return (Math.Abs(other.GetValue() - GetValue()) < 0.001f);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}