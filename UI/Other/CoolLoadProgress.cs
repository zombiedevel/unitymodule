using System.Collections;
using Architecture.Implementation.StateSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Architecture.UnityModule.UI.Other
{
	public class CoolLoadProgress : LoadingProgressBar
	{
		public Slider slider;

		public override IEnumerator OnFinish()
		{
			yield return new WaitForSeconds(0.3f);
		}

		protected override void OnChangeProgress()
		{
			slider.value = Progress;
		}
	}
}