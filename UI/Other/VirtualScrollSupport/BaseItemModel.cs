﻿using System;

namespace Architecture.UnityModule.UI.Other.VirtualScrollSupport
{
	public abstract class BaseItemModel
	{
		public Guid Identifier { get; private set; }

		public BaseItemModel()
		{
			Identifier = Guid.NewGuid();
		}
	}
}