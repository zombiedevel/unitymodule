﻿using System;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using Architecture.UnityModule.UI.ContextSystem;

namespace Architecture.UnityModule.UI.Other.VirtualScrollSupport
{
	public abstract class AbstractItemHolderNonGeneric : BaseItemContext, IBndModelInsteadDirectly
	{
		public abstract void OnDeselected();
		public abstract void OnSelected();
		public abstract BaseItemModel GetBaseModel();
		public abstract object GetModel();
		public abstract void SubscribeModelReplaced(Action a);
		public abstract void UnsubscribeModelReplaced(Action a);

		public abstract void Show();
		public abstract void Hide();
		public abstract void Select();
		public abstract void InjectItem(BaseItemModel s, bool isSelected);

		public Action<AbstractItemHolderNonGeneric> OnSelectedEvent;
	}
}