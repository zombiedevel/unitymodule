﻿using System;
using System.Collections.Generic;
using Architecture.CommonModule.ReactiveProperties;
using Architecture.UnityModule.UI.ContextSystem;
using Architecture.UnityModule.UI.Other.VirtualScrollSupport;
using UnityEngine;
using UnityEngine.UI;

public class ScrollContainerContext : SubContext
{
	[SerializeField]
	private int _cellSize = 50;
	[SerializeField]
	private int _numberOfHolders = 10;

	private RectTransform _content;
	private ScrollRect _scroll;
	[SerializeField]
	private AbstractItemHolderNonGeneric _itemPrefab;
//	private UIWidget m_containerWidget;

	private List<AbstractItemHolderNonGeneric> _itemHolders = new List<AbstractItemHolderNonGeneric>();
	private ListReactive<BaseItemModel> _itemModels = new ListReactive<BaseItemModel>();
	private Guid? m_selectedIdentifier;

	private int _needeItemsHolders;
	private int m_slotFrom;
	private int m_slotTo;

	private const int _BUFFER = 3;

	private float _initScrollWidth;

	protected override void HandlerAwake()
	{
		_scroll = GetComponent<ScrollRect>();
		_content = _scroll.content;

		_initScrollWidth = _content.sizeDelta.x;

		_needeItemsHolders = Mathf.CeilToInt(_initScrollWidth / _cellSize) + _BUFFER;

		for (int i = 0; i < _needeItemsHolders; i++)
		{
			var child = Instantiate(_itemPrefab);
			child.transform.SetParent(_scroll.content);
			child.transform.localPosition = Vector3.zero;
			_itemHolders.Add(child);
			child.OnSelectedEvent = OnHolderSelected;
			PlaceHolderToSlot(child, i);
		}

		_scroll.horizontalScrollbar.onValueChanged.AddListener((x) => Redraw());
		_scroll.verticalScrollbar.onValueChanged.AddListener((x) => Redraw());

		UpdateAllHolders();
	}

	private void OnHolderSelected(AbstractItemHolderNonGeneric selected)
	{
		foreach (var holder in _itemHolders)
		{
			holder.OnDeselected();
		}

		selected.OnSelected();
		m_selectedIdentifier = selected.GetBaseModel().Identifier;
	}

	public void Init(ListReactive<BaseItemModel> list)
	{
		_itemModels = list;
		Redraw();
		MarkCollectionChanged();
	}

	public void DeselectAll()
	{
		m_selectedIdentifier = null;
		Redraw();
	}

	public void ScrollToSelected(bool forceUpdate = false)
	{
		var index = GetSelectedIndex();
		if(!forceUpdate && (index == -1))
		{
			return;
		}

		UpdateFromToMarkers();
		const int topPadding = 200;
		var offset = m_slotFrom - index;
//		m_scrollView.MoveRelative(new Vector3(0, -offset * holderSize - topPadding, 0));
//		m_scrollView.RestrictWithinBounds(true);

		Redraw();
	}

	public T GetSelected<T>() where T : BaseItemModel
	{
		return GetSelected() as T;
	}

	public BaseItemModel GetSelected()
	{
		var index = GetSelectedIndex();
		if (index < 0)
		{
			return null;
		}
		return _itemModels[index];
	}

	public int GetSelectedIndex()
	{
		if (m_selectedIdentifier == null)
		{
			return -1;
		}

		for (int i = 0; i < _itemModels.Count; i++)
		{
			var item = _itemModels[i];
			if (m_selectedIdentifier.Value == item.Identifier)
			{
				return i;
			}
		}

		m_selectedIdentifier = null;
		return -1;
	}

	public void SelectNext()
	{
		var index = Mathf.Clamp(GetSelectedIndex() + 1, 0, _itemModels.Count - 1);
		var holder = GetHolderBySlot(index);
		if (holder != null)
		{
			holder.Select();
		}
	}

	public void SelectPrev()
	{
		var index = Mathf.Clamp(GetSelectedIndex() - 1, 0, _itemModels.Count - 1);
		var holder = GetHolderBySlot(index);
		if (holder != null)
		{
			holder.Select();
		}
	}

	public void MarkCollectionChanged()
	{
		var width = Mathf.Max(_itemModels.Count*_cellSize, _initScrollWidth);
		var oldSize = _content.sizeDelta;
		oldSize.x = width;
		_content.sizeDelta = oldSize;

		Redraw();
	}

	private void Redraw()
	{
		UpdateFromToMarkers();
		RepositionHolders();
		UpdateAllHolders();
	}

	private void UpdateAllHolders()
	{
		foreach (var holder in _itemHolders)
		{
			int index = GetHolderSlot(holder);
			if (index >= 0 && index < _itemModels.Count)
			{
				holder.Show();
				bool selected = m_selectedIdentifier != null && m_selectedIdentifier.Value == _itemModels[index].Identifier;
				holder.InjectItem(_itemModels[index], selected);
			}
			else
			{
				holder.Hide();
			}
		}
	}

	private int GetHolderSlot(AbstractItemHolderNonGeneric holder)
	{
		int fourthSize = _cellSize/4;
		int finalDistance = Convert.ToInt32(holder.transform.localPosition.x + fourthSize);
		int index = finalDistance/_cellSize;
		return index;
	}

	private void UpdateFromToMarkers()
	{
		var distance = Convert.ToInt32(- _content.anchoredPosition.x);
		int slotFromRaw = distance/_cellSize;
		m_slotFrom = slotFromRaw < 1 ? 0 : slotFromRaw - 1;
		m_slotTo = m_slotFrom + _needeItemsHolders - 1;
	}

	private void RepositionHolders()
	{
		List<int> emptySlots = GetEmptySlots();
		List<AbstractItemHolderNonGeneric> invisibleHolders = GetInvisibleHolders();

		foreach (var emptySlot in emptySlots)
		{
			if (invisibleHolders.Count == 0)
			{
				break;
			}
			var holder = invisibleHolders[0];
			invisibleHolders.RemoveAt(0);
			PlaceHolderToSlot(holder, emptySlot);
		}
	}

	private List<AbstractItemHolderNonGeneric> GetInvisibleHolders()
	{
		List<AbstractItemHolderNonGeneric> invisibleHolders = new List<AbstractItemHolderNonGeneric>();
		foreach (var holder in _itemHolders)
		{
			if (!IsVisibleHolder(holder))
			{
				invisibleHolders.Add(holder);
			}
		}
		return invisibleHolders;
	}

	private List<int> GetEmptySlots()
	{
		List<int> emptySlots = new List<int>();
		for (int i = m_slotFrom; i < m_slotTo; i++)
		{
			if (GetHolderBySlot(i) == null)
			{
				emptySlots.Add(i);
			}
		}
		return emptySlots;
	}

	private AbstractItemHolderNonGeneric GetHolderBySlot(int bySlot)
	{
		foreach (var holder in _itemHolders)
		{
			if (GetHolderSlot(holder) == bySlot)
			{
				return holder;
			}
		}
		return null;
	}

	private void PlaceHolderToSlot(AbstractItemHolderNonGeneric holder, int toSlot)
	{
		holder.GetComponent<RectTransform>().anchoredPosition = new Vector2(toSlot * _cellSize, 0);
	}

	private bool IsVisibleHolder(AbstractItemHolderNonGeneric holder)
	{
		var slot = GetHolderSlot(holder);
		return slot >= m_slotFrom && slot <= m_slotTo;
	}
}