﻿using System;
using Architecture.CommonModule.ReactiveProperties;
using Architecture.UnityModule.Extensions;
using UnityEngine;

namespace Architecture.UnityModule.UI.Other.VirtualScrollSupport
{
	public abstract class AbstractItemHolder<T> : AbstractItemHolderNonGeneric
		where T : BaseItemModel
	{
		public T Model;
		public BoolReactive IsSelected = new BoolReactive();
		public BoolReactive IsHide = new BoolReactive();
		public Action _onModelReplaced;

		public override void InjectItem(BaseItemModel s, bool isSelected)
		{
			Model = (T) s;
			IsSelected.Value = isSelected;
			if (IsHide.Value)
			{
				return;
			}
			_onModelReplaced.SafeCall();
			HandleRefresh();
		}

		public virtual void HandleRefresh()
		{
		}

		public override void Select()
		{
			OnSelectedEvent.SafeCall(this);
		}

		public override void OnSelected()
		{
			IsSelected.Value = true;
			if (IsHide.Value)
			{
				return;
			}
			HandleRefresh();
		}

		public override void OnDeselected()
		{
			IsSelected.Value = false;
			if (IsHide.Value)
			{
				return;
			}
			HandleRefresh();
		}

		public override void Show()
		{
			if (!IsHide.Value)
			{
				return;
			}

			IsHide.Value = false;
			GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
		}

		public override void Hide()
		{
			if (IsHide.Value)
			{
				return;
			}

			IsHide.Value = true;
			GetComponent<RectTransform>().localScale = new Vector3(0, 0, 0);
		}

		public override BaseItemModel GetBaseModel()
		{
			return Model;
		}

		public override object GetModel()
		{
			return Model;
		}

		public override void SubscribeModelReplaced(Action a)
		{
			_onModelReplaced += a;
		}

		public override void UnsubscribeModelReplaced(Action a)
		{
			_onModelReplaced -= a;
		}
	}
}