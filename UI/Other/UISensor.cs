﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Architecture.UnityModule.UI.Other
{
	public class UISensor : Graphic, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler
	{
		public PointerDataEvent OnBeginDragEvent = new PointerDataEvent();
		public PointerDataEvent OnDragEvent = new PointerDataEvent();
		public PointerDataEvent OnEndDragEvent = new PointerDataEvent();
		[Space]
		public PointerDataEvent OnPointerClickEvent = new PointerDataEvent();
		[Space]
		public PointerDataEvent OnPointerDownEvent = new PointerDataEvent();
		public PointerDataEvent OnPointerPressEvent = new PointerDataEvent();
		public PointerDataEvent OnPointerUpEvent = new PointerDataEvent();

		private PointerEventData _currentPointerData;

		private Vector2 _currentPosition;
		private Vector2 _onPressPosition;

		private bool m_isDragging;
		private bool m_isPressed;

		protected override void OnPopulateMesh(VertexHelper vh)
		{
			vh.Clear();
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
			OnBeginDragEvent.Invoke(eventData);
			m_isDragging = true;
		}

		public void OnDrag(PointerEventData eventData)
		{
			_currentPointerData = eventData;
			_currentPosition = eventData.position;
			OnDragEvent.Invoke(eventData);
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			OnEndDragEvent.Invoke(eventData);
			m_isDragging = false;
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			if (m_isDragging)
			{
				return;
			}

			OnPointerClickEvent.Invoke(eventData);
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			_onPressPosition = eventData.position;
			_currentPointerData = eventData;
			_currentPosition = eventData.position;

			OnPointerDownEvent.Invoke(eventData);
			m_isPressed = true;
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			OnPointerUpEvent.Invoke(eventData);
			m_isPressed = false;
		}

		public void Update()
		{
			if (m_isPressed)
			{
				OnPointerPressEvent.Invoke(_currentPointerData);
			}
		}

		public Vector2 GetStickInfo()
		{
			var result = Vector2.zero;
			if (m_isPressed)
			{
				result = _currentPosition - _onPressPosition;
			}
			return result;
		}

		public bool IsPressed()
		{
			return m_isPressed;
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			m_isPressed = false;
		}

		[Serializable]
		public class PointerDataEvent : UnityEvent<PointerEventData>
		{
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			m_isPressed = false;
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			if (eventData.pointerPress)
			{
				m_isPressed = true;
			}
		}
	}
}