﻿namespace Architecture.UnityModule.UI.ContextSystem
{
	/// <summary>
	/// Основной контекст удовлетворяющий следующим требованиям -
	/// 1 -экземпляр контекста существует в единственном числе в пределах базового контекста
	/// 2 - в одно время может быть активен только один General Context
	/// </summary>
	public class StateContext : SingleInstancedContext
	{
	}
}