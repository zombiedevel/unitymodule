﻿namespace Architecture.UnityModule.UI.ContextSystem
{
	/// <summary>
	///     Саб-контекст. Подходит для саб-экранов и итемов. Может существовать в любом количестве
	/// </summary>
	public class SubContext : BaseContext
	{
	}
}