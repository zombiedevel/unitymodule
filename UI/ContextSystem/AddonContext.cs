﻿namespace Architecture.UnityModule.UI.ContextSystem
{
	/// <summary>
	/// Контекст существующий в единственном числе в пределах контекста, но имеющий возможность быть активированным наряду с другими контекстами
	/// Примеры - панели, диалоговые окна и т.п.
	/// </summary>
	public class AddonContext : SingleInstancedContext
	{
	}
}