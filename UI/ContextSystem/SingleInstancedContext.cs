﻿namespace Architecture.UnityModule.UI.ContextSystem
{
	/// <summary>
	/// Контекст существующий в единственном экземпляре в пределах базового контекста
	/// </summary>
	public class SingleInstancedContext : BaseContext
	{
		
	}
}