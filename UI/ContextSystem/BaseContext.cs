﻿using System.Collections.Generic;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using UnityEngine;

namespace Architecture.UnityModule.UI.ContextSystem
{
	public abstract class BaseContext : MonoBehaviour, IBndTarget
	{
		public bool IsActivated { get; private set; }
		public bool IsInited { get; private set; }

		private bool _firstTimeActivate = true;

		public bool UILinked
		{
			get
			{
				return _uiRoot != null;
			}
		}

		[SerializeField]
		private List<StateContext> _stateContexts = new List<StateContext>();

		[SerializeField]
		private List<AddonContext> _addonContexts = new List<AddonContext>();

		private List<SingleInstancedContext> _instancedContexts = new List<SingleInstancedContext>();

		private readonly List<StateContext> _subStatesHistory = new List<StateContext>();

		private BaseContext _parentContext;
		private StateContext _activeSubState;
		private Transform _uiRoot;

		protected void Awake()
		{
			InitIfNeeded();
		}

		protected void Start()
		{
			HandlerStart();
		}

		protected void OnDestroy()
		{
			HandlerDestroy();
		}

		private void InitIfNeeded()
		{
			if (IsInited)
			{
				return;
			}

			_uiRoot = transform.Find("Root");
			HandlerAwake();

			IsInited = true;
		}

		protected BaseContext Parent
		{
			get
			{
				return _parentContext ?? (_parentContext = transform.parent.GetComponentInParent<BaseContext>());
			}
		}

		public void SwitchSubState<T>() where T : StateContext
		{
			var ss = Child<T>();
			SwitchSubState(ss);
		}

		public void SwitchSubState(StateContext ss)
		{
			ActivateSubstate(ss);
		}

		public void DeactivateSubState()
		{
			if (_activeSubState != null)
			{
				_activeSubState.Deactivate();
				_activeSubState = null;
			}
		}

		protected void ActivateSubstate(StateContext context, bool writeToHistory = true)
		{
			if (context == _activeSubState)
			{
				return;
			}

			if (writeToHistory)
			{
				_subStatesHistory.Add(context);
				if (_subStatesHistory.Count > 200)
				{
					_subStatesHistory.RemoveAt(0);
				}
			}

			DeactivateSubState();
			_activeSubState = context;
			_activeSubState.Activate();
			HandlerSwithSubstate( context );
		}

		public void BackState(int numberOfBack = 1)
		{
			if (_subStatesHistory.Count < numberOfBack + 1)
			{
				return;
			}

			var prevState = _subStatesHistory[_subStatesHistory.Count - numberOfBack - 1];

			if (prevState == null)
			{
				return;
			}

			for (int i = 0; i < numberOfBack; i++)
			{
				_subStatesHistory.RemoveAt(_subStatesHistory.Count - 1);
			}
			ActivateSubstate(prevState, false);
		}

		public void ActivateAddon<T>() where T : AddonContext
		{
			var comp = Child<T>();
			comp.Activate();
		}

		public void DeactivateAddon<T>() where T : AddonContext
		{
			var comp = Child<T>();
			comp.Deactivate();
		}

		public T Child<T>() where T : SingleInstancedContext
		{
			foreach (var singleInstancedContext in _instancedContexts)
			{
				if (singleInstancedContext.GetType() == typeof(T))
				{
					return singleInstancedContext as T;
				}
			}

			foreach (var context in _stateContexts)
			{
				if (context.GetType() == typeof(T))
				{
					return InstanceContext(context as T);
				}
			}

			foreach (var context in _addonContexts)
			{
				if (context.GetType() == typeof(T))
				{
					return InstanceContext(context as T);
				}
			}

			Debug.LogError("Not registred " + typeof(T) + " in " + GetType().Name);
			return null;
		}

		private T InstanceContext<T>(T context) where T : SingleInstancedContext
		{
			var ctx = Instantiate(context, _uiRoot).GetComponent<T>();
			_instancedContexts.Add(ctx);
			ctx.InitIfNeeded();
			ctx.HideUI();
			return ctx;
		}

		public T Near<T>() where T : SingleInstancedContext
		{
			return Parent.Child<T>();
		}

		public T Upper<T>() where T : SingleInstancedContext
		{
			return Parent.Near<T>();
		}

		public void Activate()
		{
			if (UILinked)
			{
				_uiRoot.gameObject.SetActive(true);
			}

			gameObject.name = GetType().Name;

			var list = GetShowAddons();
			if (list != null)
			{
				foreach (var additionContext in GetShowAddons())
				{
					additionContext.Activate();
				}
			}

			var substate = GetInitialSubState();
			if (substate != null)
			{
				ActivateSubstate(substate);
			}

			HandlerActivate();

			if (_firstTimeActivate)
			{
				HandlerFirstTimeActivate();
				_firstTimeActivate = false;
			}

			IsActivated = true;
		}

		public void Deactivate()
		{
			IsActivated = false;
			HideUI();

			var list = GetHideAddons();
			if (list != null)
			{
				foreach (var additionContext in GetHideAddons())
				{
					additionContext.Deactivate();
				}
			}

			DeactivateSubState();

			HandlerDeactivate();
		}

		public void HideUI()
		{
			if (UILinked)
			{
				_uiRoot.gameObject.SetActive(false);
			}
			gameObject.name = GetType().Name + " [hide]";
		}

		public void ShowUI()
		{
			if (UILinked)
			{
				_uiRoot.gameObject.SetActive(true);
			}
			gameObject.name = GetType().Name;
		}

		/// <summary>
		///     Вызывается когда контекст инициализируется (в Awake)
		/// </summary>
		protected virtual void HandlerAwake()
		{
		}

		/// <summary>
		///     Вызывается когда у контекста срабатывает Start()
		/// </summary>
		protected virtual void HandlerStart()
		{
		}

		/// <summary>
		///     Вызывается когда контекст уничтожается
		/// </summary>
		protected virtual void HandlerDestroy()
		{
		}

		/// <summary>
		///     Вызывается когда контекст становится видимым
		/// </summary>
		protected virtual void HandlerActivate()
		{
		}

		/// <summary>
		/// Вызывается после первой активации контекста
		/// </summary>
		protected virtual void HandlerFirstTimeActivate()
		{
		}

		/// <summary>
		///     Вызывается когда контекст скрывается
		/// </summary>
		protected virtual void HandlerDeactivate()
		{
		}

		/// <summary>
		///     Сабстейт, который активируется в самом начале
		/// </summary>
		/// <returns></returns>
		protected virtual StateContext GetInitialSubState()
		{
			return null;
		}

		/// <summary>
		///     Список контекстов которые нужно показать при активации основного контекста (по умолчанию null)
		/// </summary>
		/// <returns></returns>
		protected virtual IEnumerable<AddonContext> GetShowAddons()
		{
			return null;
		}

		/// <summary>
		///     Список контекстов которые нужно скрыть при деактивации основного контекста (по умолчанию те же, что и показываются)
		/// </summary>
		/// <returns></returns>
		protected virtual IEnumerable<AddonContext> GetHideAddons()
		{
			return GetShowAddons();
		}

		/// <summary>
		///     Вызывается когда контекст скрывается
		/// </summary>
		protected virtual void HandlerSwithSubstate(StateContext context)
		{
		}

		public string GetName()
		{
			return gameObject.name;
		}
	}
}