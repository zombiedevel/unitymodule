﻿using System;

namespace Architecture.UnityModule.UI.BndSystem.Components.Base
{
	public interface IBndModelInsteadDirectly
	{
		object GetModel();
		void SubscribeModelReplaced(Action a);
		void UnsubscribeModelReplaced(Action a);
	}
}