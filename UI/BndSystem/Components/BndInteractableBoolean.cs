﻿using Architecture.CommonModule.ReactiveProperties;
using Architecture.UnityModule.UI.BndSystem.Components.Base;

namespace Architecture.UnityModule.UI.BndSystem.Components
{
	public class BndInteractableBoolean : BaseBndInteractable
	{
		private BoolReactive _agentBoolean;

		protected override void InitializeProperties()
		{
			_agentBoolean = InitProperty<BoolReactive>(PropName);
		}

		protected override void OnChangedProperties()
		{
			SetInteractable(_agentBoolean.Value);
		}
	}
}