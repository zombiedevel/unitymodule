﻿using Architecture.CommonModule.ReactiveProperties;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Architecture.UnityModule.UI.BndSystem.Components
{
	[RequireComponent(typeof(Image))]
	public class BndSpriteAlpha : BaseBndComponent
	{
		private FloatReactive _agent;
		private Image _image;

		[SerializeField] private string _propName;

		protected override void InitializeProperties()
		{
			_agent = InitProperty<FloatReactive>(_propName);
		}

		protected override void InitializeComponent()
		{
			_image = GetComponent<Image>();
		}

		protected override void OnChangedProperties()
		{
			var color = _image.color;
			color.a = _agent.Value;
			_image.color = color;
		}
	}
}