﻿using Architecture.CommonModule.ReactiveProperties.Base;
using Architecture.UnityModule.Extensions;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using UnityEngine;

namespace Architecture.UnityModule.UI.BndSystem.Components
{
	[RequireComponent(typeof(RectTransform))]
	public class BndVisibilityNumeric : BaseBndVisibility
	{
		public enum TypeBinding
		{
			LessOrEqual = 0,
			Equal = 1,
			GreaterOrEqual = 2
		}

		[SerializeField] private float _point;
		[SerializeField] private TypeBinding _typeBinding = TypeBinding.Equal;

		private INumericProperty _agentNumeric;

		protected override void InitializeProperties()
		{
			_agentNumeric = InitProperty<ReactiveProperty>(PropName) as INumericProperty;
		}

		protected override void OnChangedProperties()
		{
			var visibility = false;

			if (_agentNumeric == null)
			{
				return;
			}

			switch (_typeBinding)
			{
				case TypeBinding.LessOrEqual:
					visibility = _agentNumeric.GetFloatValue() <= _point;
					break;
				case TypeBinding.Equal:
					visibility = _agentNumeric.GetFloatValue().IsEqualTolerance(_point);
					break;
				case TypeBinding.GreaterOrEqual:
					visibility = _agentNumeric.GetFloatValue() >= _point;
					break;
			}
			SetVisible(visibility);
		}
	}
}