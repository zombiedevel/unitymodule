﻿using Architecture.CommonModule.ReactiveProperties.Base;
using Architecture.UnityModule.Extensions;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using UnityEngine;

namespace Architecture.UnityModule.UI.BndSystem.Components
{
	public class BndInteractableNumeric : BaseBndInteractable
	{
		public enum TypeBinding
		{
			LessOrEqual = 0,
			Equal = 1,
			GreaterOrEqual = 2
		}

		private INumericProperty _agent;

		[SerializeField]
		private TypeBinding _typeBinding = TypeBinding.Equal;
		[SerializeField]
		private float _point;

		protected override void InitializeProperties()
		{
			_agent = InitProperty<ReactiveProperty>(PropName) as INumericProperty;
		}

		protected override void OnChangedProperties()
		{
			var visibility = false;

			switch (_typeBinding)
			{
				case TypeBinding.LessOrEqual:
					visibility = _agent.GetFloatValue() <= _point;
					break;
				case TypeBinding.Equal:
					visibility = _agent.GetFloatValue().IsEqualTolerance(_point);
					break;
				case TypeBinding.GreaterOrEqual:
					visibility = _agent.GetFloatValue() >= _point;
					break;
			}

			SetInteractable(visibility);
		}
	}
}