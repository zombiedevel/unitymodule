﻿using Architecture.CommonModule.ReactiveProperties;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

namespace Architecture.UnityModule.UI.BndSystem.Components
{
	[RequireComponent(typeof(Image))]
	public class BndSpriteAtlas : BaseBndComponent
	{
		private StringReactive _agent;

		[SerializeField]
		private SpriteAtlas _atlas;

		private Image _image;

		[SerializeField] private string _propName;

		protected override void InitializeProperties()
		{
			_agent = InitProperty<StringReactive>(_propName);
		}

		protected override void InitializeComponent()
		{
			_image = GetComponent<Image>();
		}

		protected override void OnChangedProperties()
		{
			_image.sprite = _atlas.GetSprite(_agent.Value);
		}
	}
}