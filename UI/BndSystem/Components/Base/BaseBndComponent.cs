﻿using System;
using System.Collections.Generic;
using Architecture.CommonModule.ReactiveProperties.Base;
using Architecture.UnityModule.Extensions;
using UnityEngine;

namespace Architecture.UnityModule.UI.BndSystem.Components.Base
{
	public abstract class BaseBndComponent : MonoBehaviour
	{
		private readonly List<ReactiveProperty> _allProperties = new List<ReactiveProperty>();

		protected IBndTarget TargetContext { get; private set; }
		protected object TargetModel { get; private set; }

		private void InitSource()
		{
			if (TargetContext == null)
			{
				TargetContext = gameObject.FindInParents<IBndTarget>();
			}

			bool takeFromModel = TargetContext is IBndModelInsteadDirectly;
			if (takeFromModel)
			{
				var targ = (IBndModelInsteadDirectly) TargetContext;
				TargetModel = targ.GetModel();
				targ.SubscribeModelReplaced(ReloadBindedInfo);
			}
			else
			{
				TargetModel = TargetContext;
			}
		}

		protected void Start()
		{
			InitializeComponent();
			ReloadBindedInfo();
		}

		protected void ReloadBindedInfo()
		{
			// if not first time
			if (TargetContext != null)
			{
				ClearAllSubscribes();
			}

			InitSource();
			InitializeProperties();
			OnChangedProperties();
		}

		protected void OnDestroy()
		{
			ClearAllSubscribes();
		}

		private void ClearAllSubscribes()
		{
			foreach (var prop in _allProperties)
			{
				prop.OnChanged -= OnChangedProperties;
			}
			_allProperties.Clear();

			if (TargetContext is IBndModelInsteadDirectly)
			{
				var source = (IBndModelInsteadDirectly) TargetContext;
				source.UnsubscribeModelReplaced(ReloadBindedInfo);
			}
		}

		protected T InitProperty<T>(string propertyName) where T : ReactiveProperty, new()
		{
			try
			{
				// if model not loaded yet
				if (TargetModel == null)
				{
					return new T();
				}

				var type = TargetModel.GetType();
				var fieldInfo = type.GetField(propertyName);
				if (fieldInfo == null)
				{
					return new T();
				}

				var m = fieldInfo.GetValue(TargetModel);
				var property = (T) m;

				_allProperties.Add(property);
				property.OnChanged += OnChangedProperties;
				return property;
			}
			catch (Exception e)
			{
				ErrorLog(propertyName, e.Message);
				return new T();
			}
		}

		private void ErrorLog(string propertyName, string add)
		{
			Debug.LogError("Error bind!\n" +
							"GameObject: " + gameObject.name + "\n" +
							"BndComponent: " + GetType().FullName + "\n" +
							"Property: " + propertyName + "\n" +
							"TargetContext: " + TargetContext.GetType().FullName + "\n"
							+ add);
		}

		protected abstract void InitializeComponent();
		protected abstract void InitializeProperties();
		protected abstract void OnChangedProperties();
	}
}