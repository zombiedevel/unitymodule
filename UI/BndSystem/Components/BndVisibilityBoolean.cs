﻿using Architecture.CommonModule.ReactiveProperties;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using UnityEngine;

namespace Architecture.UnityModule.UI.BndSystem.Components
{
	[RequireComponent(typeof(RectTransform))]
	public class BndVisibilityBoolean : BaseBndVisibility
	{
		private BoolReactive _agentBoolean;

		protected override void InitializeProperties()
		{
			_agentBoolean = InitProperty<BoolReactive>(PropName);
		}

		protected override void OnChangedProperties()
		{
			SetVisible(_agentBoolean.Value);
		}
	}
}