﻿using Architecture.CommonModule.ReactiveProperties.Base;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Architecture.UnityModule.UI.BndSystem.Components
{
	[RequireComponent(typeof(Text))]
	public class BndText : BaseBndText
	{
		private ReactiveProperty _basicAgent;

		[SerializeField] private string _propName = "";

		protected override void InitializeProperties()
		{
			_basicAgent = InitProperty<ReactiveProperty>(_propName);
		}

		protected override void OnChangedProperties()
		{
			TextComponent.text = string.Format(Format, _basicAgent);
		}
	}
}