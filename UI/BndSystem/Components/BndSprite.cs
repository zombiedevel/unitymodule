﻿using Architecture.CommonModule.ReactiveProperties;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Architecture.UnityModule.UI.BndSystem.Components
{
	[RequireComponent(typeof(Image))]
	public class BndSprite : BaseBndComponent
	{
		private SpriteReactive _agent;

		private Image _image;

		[SerializeField] private string _propName;

		protected override void InitializeProperties()
		{
			_agent = InitProperty<SpriteReactive>(_propName);
		}

		protected override void InitializeComponent()
		{
			_image = GetComponent<Image>();
		}

		protected override void OnChangedProperties()
		{
			_image.sprite = _agent.Value;
		}
	}
}