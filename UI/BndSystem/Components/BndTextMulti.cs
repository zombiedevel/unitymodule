﻿using System.Collections.Generic;
using Architecture.CommonModule.ReactiveProperties.Base;
using Architecture.Implementation.Localization;
using Architecture.UnityModule.UI.BndSystem.Components.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Architecture.UnityModule.UI.BndSystem.Components
{
	[RequireComponent(typeof(Text))]
	public class BndTextMulti : BaseBndText
	{
		private readonly List<ReactiveProperty> _propAgents = new List<ReactiveProperty>();

		[SerializeField] private List<string> _propNames;
		private object[] _propValues;

		protected override void InitializeProperties()
		{
			_propAgents.Clear();
			_propValues = new object[_propNames.Count];
			for (var i = 0; i < _propNames.Count; i++)
			{
				var propName = _propNames[i];
				if (propName.StartsWith("LangKey:"))
				{
					_propValues[i] = propName.Replace("LangKey:", "").Localized();
					_propAgents.Add(null);
				}
				else if (propName.StartsWith("LangKeyUpper:"))
				{
					_propValues[i] = propName.Replace("LangKeyUpper:", "").Localized().ToUpperInvariant();
					_propAgents.Add(null);
				}
				else
				{
					_propAgents.Add(InitProperty<ReactiveProperty>(propName));
				}
			}
		}

		protected override void OnChangedProperties()
		{
			for (var i = 0; i < _propAgents.Count; i++)
			{
				var agent = _propAgents[i];
				if (agent == null)
				{
					continue;
				}
				_propValues[i] = agent.ToString();
			}
			TextComponent.text = string.Format(Format, _propValues);
		}
	}
}