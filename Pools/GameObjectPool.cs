﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Architecture.UnityModule.Pools
{
	public class GameObjectPool : MonoBehaviour
	{
		public static GameObjectPool Instance;
		private readonly List<GameObject> _pooledObjects = new List<GameObject>();

		private static readonly int MAX_SIZE = 50;

		private void Awake()
		{
			Instance = this;
		}

		private void OnDestroy()
		{
			Instance = null;
		}

		public void Reserve(GameObject source, int count)
		{
			for (var i = 0; i < count; i++)
			{
				var go = Instantiate(source);
				go.name = go.name.Substring(0, go.name.IndexOf('('));
				ToPool(go);
			}
		}

		public GameObject Get(GameObject prefab)
		{
			var go = _pooledObjects.Find(v => v.name == prefab.name);
			if (go == null)
			{
				go = Instantiate(prefab);
				go.name = go.name.Substring(0, go.name.IndexOf('('));
			}
			else
			{
				_pooledObjects.Remove(go);
				go.transform.SetParent(null, false);
				go.SetActive(true);
			}

			return go;
		}

		public void ToPool(GameObject go, float delay)
		{
			StartCoroutine(ToPoolDelay(go, delay));
		}

		private IEnumerator ToPoolDelay(GameObject go, float delay)
		{
			yield return new WaitForSeconds(delay);
			ToPool(go);
		}

		public void ToPool(GameObject go)
		{
			if (go == null)
			{
				return;
			}

			if (_pooledObjects.Count >= MAX_SIZE)
			{
				// если такой объект уже есть в пуле, удаляем тот который пытаемся вернуть в пул
				// и больше ничего не делаем
				foreach (var pooledObject in _pooledObjects)
				{
					if (pooledObject.gameObject.name == go.name)
					{
						Destroy(go);
						return;
					}
				}

				// если же такого объекта в пуле нет, то удаляем самый старый объект из пула
				var m = _pooledObjects[0];
				_pooledObjects.RemoveAt(0);
				if (m != null)
				{
					Destroy(m);
				}
			}

			// добавляем новый объект в пул
			go.SetActive(false);
			go.transform.SetParent(transform, false);
			_pooledObjects.Add(go);
		}
	}
}