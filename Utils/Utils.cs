﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Architecture.UnityModule.Utils
{
	public static class Utils
	{
		private static int seed = DateTime.Now.Second;

		public static void Shuffle<T>(this T[] array)
		{
			int count = array.Length, i, randIndex;
			for (i = count - 1; i > 0; --i)
			{
				randIndex = UnityEngine.Random.Range(0, i);
				var temp = array[i];
				array[i] = array[randIndex];
				array[randIndex] = temp;
			}
		}

		// перемешивание так, чтоб не было повторов после окончания, последний никогда не станет первым
		public static void ShuffleContinues<T>(this T[] array)
		{
			int count = array.Length, i, randIndex;
			var lastIndex = count - 1;
			for (i = count - 1; i > 0; --i)
			{
				randIndex = UnityEngine.Random.Range(0, i);
				if (i == lastIndex)
				{
					lastIndex = randIndex;
				}
				else if (randIndex == lastIndex)
				{
					lastIndex = i;
				}
				var temp = array[i];
				array[i] = array[randIndex];
				array[randIndex] = temp;
			}

			// последний перекинем куда нибудь с 0й позиции
			if (lastIndex == 0)
			{
				randIndex = UnityEngine.Random.Range(1, count);
				var temp = array[0];
				array[0] = array[randIndex];
				array[randIndex] = temp;
			}
		}

		public static void SetAlpha(this Graphic g, float a)
		{
			var c = g.color;
			c.a = a;
			g.color = c;
		}

		public static Rect Scale(this Rect r, float s)
		{
			return new Rect(r.x * s, r.y * s, r.width * s, r.height * s);
		}

		public static Rect OffsetToCenter(this Rect r, float offsetX, float offsetY)
		{
			return new Rect(r.x + offsetX, r.y + offsetY, r.width - offsetX * 2, r.height - offsetY * 2);
		}

		public static Rect GetScreenRect(this RectTransform rt)
		{
			var r = rt.rect;
			r.x = rt.anchoredPosition.x;
			r.y = rt.anchoredPosition.y;
			return r.Scale(rt.lossyScale.x);
		}

		public static Rect GetScreenRect2(this RectTransform rectTransform)
		{
			var corners = new Vector3[4];

			rectTransform.GetWorldCorners(corners);

			var xMin = float.PositiveInfinity;
			var xMax = float.NegativeInfinity;
			var yMin = float.PositiveInfinity;
			var yMax = float.NegativeInfinity;

			for (var i = 0; i < 4; i++)
			{
				// For Canvas mode Screen Space - Overlay there is no Camera; best solution I've found
				// is to use RectTransformUtility.WorldToScreenPoint) with a null camera.

				Vector3 screenCoord = RectTransformUtility.WorldToScreenPoint(null, corners[i]);

				if (screenCoord.x < xMin)
				{
					xMin = screenCoord.x;
				}
				if (screenCoord.x > xMax)
				{
					xMax = screenCoord.x;
				}
				if (screenCoord.y < yMin)
				{
					yMin = screenCoord.y;
				}
				if (screenCoord.y > yMax)
				{
					yMax = screenCoord.y;
				}
			}

			var result = new Rect(xMin, yMin, xMax - xMin, yMax - yMin);

			return result;
		}

		public static float RangeRand(this Vector2 v)
		{
			return Rnd.Next(v.x, v.y);
		}

		public static Vector3 GetRandomDirVectorInCone(float angleRadians)
		{
			UnityEngine.Random.InitState(seed++);

			var circlePoint = UnityEngine.Random.insideUnitCircle * Mathf.Sin(angleRadians);
			var z = Mathf.Sqrt(1 - circlePoint.sqrMagnitude);
			return new Vector3(circlePoint.x, circlePoint.y, z);
		}

		[Serializable]
		public class SinCosAnimator
		{
			private float timeX = Mathf.PI / 2;
			private float timeY;
			public float xRadius = 0;
			public float xTimeScale = 1;
			public float yRadius = 0;
			public float yTimeScale = 1;

			public Vector2 Update(float dt)
			{
				timeX += dt * xTimeScale;
				timeY += dt * yTimeScale;
				return new Vector2(Mathf.Cos(timeX) * xRadius, Mathf.Sin(timeY) * yRadius);
			}
		}
	}
}