﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace Architecture.UnityModule.Utils
{
	public static class CustomDES
	{
		private const int _ITERATIONS = 200;

		public static string Encrypt(string plainText, string password)
		{
			if (string.IsNullOrEmpty(plainText))
			{
				return "";
			}

			if (string.IsNullOrEmpty(password))
			{
				Debug.LogError("Password is not valid. Returned empty");
				return "";
			}

			var des = new DESCryptoServiceProvider();
			des.GenerateIV();
			var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, des.IV, _ITERATIONS);
			var key = rfc2898DeriveBytes.GetBytes(8);
			using (var memoryStream = new MemoryStream())
			{
				using (var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(key, des.IV), CryptoStreamMode.Write))
				{
					memoryStream.Write(des.IV, 0, des.IV.Length);
					var bytes = Encoding.UTF8.GetBytes(plainText);
					cryptoStream.Write(bytes, 0, bytes.Length);
					cryptoStream.FlushFinalBlock();
					return CustomSurprise(Convert.ToBase64String(memoryStream.ToArray()));
				}
			}
		}

		private static string CustomSurprise(string s)
		{
			var newArr = new char[s.Length];
			for (var i = 0; i < s.Length; i++)
			{
				if (i >= 2 && i < s.Length - 2)
				{
					newArr[i] = s[s.Length - 1 - i];
				}
				else
				{
					newArr[i] = s[i];
				}
			}
			return new string(newArr);
		}

		public static string Decrypt(string cipherText, string password)
		{
			cipherText = CustomSurprise(cipherText);
			if (string.IsNullOrEmpty(cipherText))
			{
				return "";
			}

			if (string.IsNullOrEmpty(password))
			{
				Debug.LogError("Password is not cannot empty. Returned empty");
				return "";
			}

			try
			{
				var cipherBytes = Convert.FromBase64String(cipherText);

				using (var memoryStream = new MemoryStream(cipherBytes))
				{
					var des = new DESCryptoServiceProvider();
					var iv = new byte[8];
					memoryStream.Read(iv, 0, iv.Length);
					var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, iv, _ITERATIONS);
					var key = rfc2898DeriveBytes.GetBytes(8);
					using (var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(key, iv), CryptoStreamMode.Read))
					{
						using (var streamReader = new StreamReader(cryptoStream))
						{
							return streamReader.ReadToEnd();
						}
					}
				}
			}
			catch (Exception ex)
			{
				Debug.LogError("Exception: " + ex.Message + " from decrypt. Returned empty");
				return "";
			}
		}
	}
}