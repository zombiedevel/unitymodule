﻿using System;
using System.Collections;
using Architecture.UnityModule.Extensions;
using UnityEngine;

namespace Architecture.UnityModule.Utils
{
	public static class CoroutineHelper
	{
		private class CoroutineGo : MonoBehaviour
		{
			public void OnDestroy()
			{
				Clean();
			}
		}

		private static CoroutineGo _coroutineGo;

		private static void InitIfNeeded()
		{
			if (_coroutineGo == null)
			{
				_coroutineGo = new GameObject().AddComponent<CoroutineGo>();
				_coroutineGo.gameObject.name = "CoroutineHelper";
			}
		}

		private static void Clean()
		{
			_coroutineGo = null;
		}

		public static void FromTo(float duration, float from, float to, Action<float> setNumAction, Action onStart = null,
			Action onEnd = null)
		{
			InitIfNeeded();
			_coroutineGo.StartCoroutine(FromToCoroutine(duration, from, to, setNumAction, onStart, onEnd));
		}

		private static IEnumerator FromToCoroutine(float duration, float from, float to, Action<float> setNumAction,
			Action onStart, Action onEnd)
		{
			onStart.SafeCall();

			var outNum = from;
			setNumAction.SafeCall(outNum);
			var min = Mathf.Min(from, to);
			var max = Mathf.Max(from, to);
			var timeFrom = Time.realtimeSinceStartup;
			var numOnSecond = (to - from) / duration;
			while (Math.Abs(outNum - to) > 0.01)
			{
				var diffTime = Time.realtimeSinceStartup - timeFrom;
				outNum = from + diffTime * numOnSecond;
				outNum = Mathf.Clamp(outNum, min, max);
				setNumAction.SafeCall(outNum);
				yield return null;
			}
			outNum = to;
			setNumAction.SafeCall(outNum);

			onEnd.SafeCall();
		}

		public static void FromCurve(float duration, AnimationCurve curve, Action<float> setNumAction, Action onStart = null,
			Action onEnd = null)
		{
			InitIfNeeded();
			_coroutineGo.StartCoroutine(FromCurveCoroutine(duration, curve, setNumAction, onStart, onEnd));
		}

		private static IEnumerator FromCurveCoroutine(float duration, AnimationCurve curve, Action<float> setNumAction,
			Action onStart, Action onEnd)
		{
			onStart.SafeCall();

			var timeFrom = Time.realtimeSinceStartup;
			while (Time.realtimeSinceStartup - timeFrom < duration)
			{
				setNumAction.SafeCall(curve.Evaluate(Time.realtimeSinceStartup - timeFrom));
				yield return null;
			}

			onEnd.SafeCall();
		}

		public static void RunActionDelay(float delay, Action action)
		{
			InitIfNeeded();
			_coroutineGo.StartCoroutine(RunActionDelayCoroutine(delay, action));
		}

		private static IEnumerator RunActionDelayCoroutine(float delay, Action action)
		{
			if (delay < 0.001)
			{
				yield return null;
				yield return null;
			}
			yield return new WaitForSeconds(delay);
			action.SafeCall();
		}
	}
}