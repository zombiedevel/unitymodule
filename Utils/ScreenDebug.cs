﻿using System.Collections.Generic;
using UnityEngine;

namespace Architecture.UnityModule.Utils
{
	public static class ScreenDebug
	{
		private static int m_counter;
		private static readonly List<string> m_logList = new List<string>();
		private static GameObject m_screenConsoleGameObject;
		private static ScreenConsole m_screenConsole;
		private const int _CASHE_RECORD_SIZE = 1000;

		public static void Log(object logString)
		{
			Debug.Log("ScreenDebug: " + logString);
			var output = logString.ToString();
			var splited = output.Split('\n');
			for (var i = 0; i < splited.Length; i++)
			{
				string txt;
				if (i == 0)
				{
					txt = ++m_counter + ":" + splited[i] + "\n";
				}
				else
				{
					txt = splited[i] + "\n";
				}
				m_logList.Add(txt);
			}

			while (m_logList.Count > _CASHE_RECORD_SIZE)
			{
				m_logList.RemoveAt(0);
			}
			if (m_screenConsole == null)
			{
				Init();
			}
			m_screenConsole.HandleChangeLogList(splited.Length);
		}

		public static void LogEnumerable<T>(IEnumerable<T> objects)
		{
			var i = 0;
			foreach (var obj in objects)
			{
				Log("i" + i++ + "." + obj);
			}
		}

		private static void Init()
		{
			if (m_screenConsoleGameObject == null)
			{
				m_screenConsoleGameObject = new GameObject("ScreenDebug");
			}
			m_screenConsole = m_screenConsoleGameObject.AddComponent<ScreenConsole>();
		}

		public static void Clear()
		{
			m_logList.Clear();
			Object.Destroy(m_screenConsole);
		}

		private class ScreenConsole : MonoBehaviour
		{
			private static string m_log = "";
			private static GUIStyle m_upperCenterStyle;
			private static GUIStyle m_lowerLeftStyle;
			private const int _MAX_LINES = 4;
			private const int _BUTTON_AND_LABEL_COUNTER = 5;
			private static readonly float m_fontSize = 14f / 360 * Screen.height;
			private static readonly float m_heightFactor = 0.3f;
			private static readonly float m_widthFactor = 0.5f;
			private static readonly float m_bottomPanelHeight = Screen.height * 0.08f;
			private static readonly float m_buttonWidth = Screen.width * m_widthFactor / _BUTTON_AND_LABEL_COUNTER;
			private static readonly float m_logWPos = (Screen.width - Screen.width * m_widthFactor) / 2f;
			private static readonly float m_logHPos = Screen.height - Screen.height * m_heightFactor;
			private static bool m_isScroll = true;
			private static int m_index;
			private static int m_prevSize;
			private static bool m_init;

			private void Awake()
			{
				DontDestroyOnLoad(this);
			}

			public void HandleChangeLogList(int countLines)
			{
				if (m_prevSize == _CASHE_RECORD_SIZE)
				{
					if (!m_isScroll)
					{
						m_index -= countLines;
					}
				}
				else
				{
					if (m_isScroll)
					{
						m_index += countLines;
					}
				}
				m_prevSize = m_logList.Count;
			}

			private void OnGUI()
			{
				if (!m_init)
				{
					var labelStyle = GUI.skin.GetStyle("Label");
					m_upperCenterStyle = new GUIStyle(labelStyle) {alignment = TextAnchor.UpperCenter};
					m_lowerLeftStyle = new GUIStyle(labelStyle) {alignment = TextAnchor.LowerLeft};
					m_init = true;
				}
				var maxPermissibleStartIndex = Mathf.Max(m_logList.Count - _MAX_LINES, 0);
				m_index = Mathf.Clamp(m_index, 0, maxPermissibleStartIndex);

				var maxPermissibleEndIndex = Mathf.Max(m_logList.Count - 1, 0);
				var endIndex = Mathf.Clamp(m_index + _MAX_LINES - 1, 0, maxPermissibleEndIndex);

				m_log = string.Empty;
				try
				{
					for (var index = m_index; index <= endIndex; index++)
					{
						var s = m_logList[index];
						m_log += s;
					}
				}
				catch
				{
				}

				GUI.Label(new Rect(
						m_logWPos,
						m_logHPos,
						Screen.width * m_widthFactor,
						Screen.height * m_heightFactor - m_bottomPanelHeight),
					"<color=blue><size=" + m_fontSize + ">" + m_log + "</size></color>",
					m_lowerLeftStyle
				);
				GUI.VerticalScrollbar(new Rect(
						m_logWPos - Screen.width * 0.02f,
						Screen.height - m_bottomPanelHeight,
						Screen.width * 0.02f,
						m_bottomPanelHeight),
					m_index + _MAX_LINES / 2, Mathf.Min(_MAX_LINES, m_logList.Count), 0, m_logList.Count
				);
				if (GUI.RepeatButton(new Rect(m_logWPos, Screen.height - m_bottomPanelHeight, m_buttonWidth, m_bottomPanelHeight),
					"up"))
				{
					m_index--;
				}
				if (GUI.RepeatButton(
					new Rect(m_logWPos + m_buttonWidth, Screen.height - m_bottomPanelHeight, m_buttonWidth, m_bottomPanelHeight),
					"down"))
				{
					m_index++;
				}
				if (GUI.Button(
					new Rect(m_logWPos + m_buttonWidth * 2, Screen.height - m_bottomPanelHeight, m_buttonWidth, m_bottomPanelHeight),
					m_isScroll ? "scrl" : "stop"))
				{
					m_isScroll = !m_isScroll;
				}
				if (GUI.Button(
					new Rect(m_logWPos + m_buttonWidth * 3, Screen.height - m_bottomPanelHeight, m_buttonWidth, m_bottomPanelHeight),
					"end"))
				{
					m_isScroll = true;
					m_index = m_logList.Count - _MAX_LINES;
				}
				if (GUI.Button(
					new Rect(m_logWPos + m_buttonWidth * 4, Screen.height - m_bottomPanelHeight, m_buttonWidth, m_bottomPanelHeight),
					"clr"))
				{
					Clear();
				}
			}
		}
	}
}