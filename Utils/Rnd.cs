﻿using System;

namespace Architecture.UnityModule.Utils
{
	public static class Rnd
	{
		public static Random rnd = new Random();

		public static float Next(float from, float to)
		{
			return Convert.ToSingle(rnd.NextDouble() * (to - from) + from);
		}

		public static int Next(int from, int to)
		{
			return rnd.Next(from, to);
		}

		public static bool Bool()
		{
			return rnd.Next(0, 2) == 0;
		}
	}
}