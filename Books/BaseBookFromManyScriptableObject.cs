﻿using System.Collections.Generic;
using UnityEngine;

namespace Architecture.UnityModule.Books
{
	public abstract class BaseBookFromManyScriptableObject<T>
		where T : ScriptableObject
	{
		private Dictionary<string, T> _sourceDic;

		public T this[string key]
		{
			get
			{
				return _sourceDic[key];
			}
		}

		public void AddElementOnInit(string key, T element)
		{
			if (_sourceDic == null)
			{
				_sourceDic = new Dictionary<string, T>();
			}
			_sourceDic.Add(key, element);
		}

		public Dictionary<string, T> GetDictionary()
		{
			return _sourceDic;
		}
	}
}