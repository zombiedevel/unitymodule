﻿using System;
using System.IO;
using Architecture.UnityModule.Serializer;
using UnityEngine;

namespace Architecture.UnityModule.Books
{
	public static class BooksCore
	{
		public static T GetBookFromJson<T>(string path)
		{
			var text = Resources.Load<TextAsset>("Books/" + path).text;
			return Parser.Deserialize<T>(text);
		}

		public static T GetBookFromScriptableObject<T>(string path) where T : ScriptableObject
		{
			return Resources.Load<T>("Books/" + path);
		}

		public static T_BOOK GetBookFromManyScriptableObject<T_BOOK, T_ELEMENT>(string path) 
			where T_ELEMENT : ScriptableObject
			where T_BOOK : BaseBookFromManyScriptableObject<T_ELEMENT>
		{
			T_BOOK book = Activator.CreateInstance<T_BOOK>();

			foreach (T_ELEMENT element in Resources.LoadAll<T_ELEMENT>("Books/" + path + "/"))
			{
				book.AddElementOnInit(element.name, element);
			}
			return book;
		}

		public static void SaveBookToJson(object obj, string path)
		{
			var output = Parser.Serialize(obj, true);
			File.WriteAllText(Application.dataPath + "/Resources/Books/" + path + ".json", output);
		}
	}
}