﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Architecture.UnityModule.Serializer
{
	public static class CSV
	{
		public sealed class CsvIgnoreAttribute : Attribute
		{
		}

		private enum TypeMode
		{
			SingleSimple = 0,
			ManySimple = 1,
			DictionarySimple = 3,
			NonSuported = 4
		}

		private class PropertyAndFields
		{
			public string[] FieldNames { get; private set; }
			public string[] PropertyNames { get; private set; }

			public PropertyAndFields(string[] fieldNames, string[] propertyNames)
			{
				if (fieldNames == null)
				{
					fieldNames = new string[0];
				}
				if (propertyNames == null)
				{
					propertyNames = new string[0];
				}
				FieldNames = fieldNames;
				PropertyNames = propertyNames;
			}
		}

		private static readonly Dictionary<Type, PropertyAndFields> _specTypesList = new Dictionary<Type, PropertyAndFields>
		{
			{typeof (Vector3), new PropertyAndFields(new[] {"x", "y", "z"}, null)},
			{typeof (KeyValuePair<,>), new PropertyAndFields(null, new[] {"Key", "Value"})}
		};

		private static bool CheckNonIgnoreAttribute(MemberInfo member)
		{
			return member.GetCustomAttributes(typeof (CsvIgnoreAttribute), true).Length == 0;
		}

		private static readonly Dictionary<Type, TypeMap> _cashedTypeMaps = new Dictionary<Type, TypeMap>();

		private static TypeMap GetTypeMap(Type type)
		{
			if (_cashedTypeMaps.ContainsKey(type))
			{
				return _cashedTypeMaps[type];
			}

			TypeMap typeMap = new TypeMap();

			List<ValueMember> members = new List<ValueMember>();
			if (!_specTypesList.ContainsKey(type))
			{
				PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
				IEnumerable<PropertyInfo> nonIgnoredProperties
					= properties.Where(CheckNonIgnoreAttribute);
				foreach (var m in nonIgnoredProperties)
				{
					members.Add(new ValueMember(m, null));
				}

				FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public);
				IEnumerable<FieldInfo> nonIgnoredFields
					= fields.Where(CheckNonIgnoreAttribute);
				foreach (var m in nonIgnoredFields)
				{
					members.Add(new ValueMember(null, m));
				}
			}
			else
			{
				PropertyAndFields pAndF = _specTypesList[type];
				foreach (var pName in pAndF.PropertyNames)
				{
					members.Add(new ValueMember(type.GetProperty(pName), null));
				}
				foreach (var fName in pAndF.FieldNames)
				{
					members.Add(new ValueMember(null, type.GetField(fName)));
				}
			}

			foreach (var p in members)
			{
				var pType = p.MemberType;
				Type targetType;
				TypeMode typeMode = GetTypeMode(pType, out targetType);
				switch (typeMode)
				{
					case TypeMode.SingleSimple:
						typeMap.SingleSimple.Add(p);
						break;
					case TypeMode.ManySimple:
						typeMap.ManySimple.Add(p, targetType);
						break;
					case TypeMode.DictionarySimple:
						Error("Not implemented");
						break;
					case TypeMode.NonSuported:
						break;
				}
			}
			_cashedTypeMaps.Add(type, typeMap);
			return typeMap;
		}

		private static readonly List<Type> _simpleTypes = new List<Type>
		{
			typeof (int),
			typeof (float),
			typeof (string),
			typeof (bool)
		};

		private static TypeMode GetTypeMode(Type pType, out Type nestedType)
		{
			nestedType = pType;
			if (_simpleTypes.Contains(pType) || pType.IsEnum)
			{
				return TypeMode.SingleSimple;
			}
			if (pType.IsArray)
			{
				var elementType = pType.GetElementType();
				if (elementType == null)
				{
					Error("Element type is null");
					return TypeMode.NonSuported;
				}
				nestedType = elementType;
				if (_simpleTypes.Contains(elementType) || elementType.IsEnum)
				{
					return TypeMode.ManySimple;
				}
				return TypeMode.NonSuported;
			}
			if (pType.IsGenericType)
			{
				Type gType = pType.GetGenericTypeDefinition();
				if (gType == typeof (List<>))
				{
					var genericArgument = pType.GetGenericArguments()[0];
					nestedType = genericArgument;
					if (_simpleTypes.Contains(genericArgument) || genericArgument.IsEnum)
					{
						return TypeMode.ManySimple;
					}
					return TypeMode.NonSuported;
				}
				if (gType == typeof (Dictionary<,>))
				{
					nestedType = typeof (KeyValuePair<,>).MakeGenericType(pType.GetGenericArguments());
					return TypeMode.DictionarySimple;
				}
				Error("Non supported generic type " + pType.Name);
				return TypeMode.NonSuported;
			}
			if (pType.IsPrimitive)
			{
				Error("Not supported primitive type " + pType.Name);
				return TypeMode.NonSuported;
			}
			Error("Not supported type " + pType.Name);
			return TypeMode.NonSuported;
		}

		private class ValueMember
		{
			private readonly PropertyInfo _property;
			private readonly FieldInfo _field;

			public ValueMember(PropertyInfo prop, FieldInfo field)
			{
				_property = prop;
				_field = field;
			}

			public void SetValue<T>(ref T obj, object value)
			{
				object boxed = obj;
				if (_property != null)
				{
					_property.SetValue(boxed, value, null);
				}
				else
				{
					_field.SetValue(boxed, value);
				}
				obj = (T) boxed;
			}

			public object GetValue(object obj)
			{
				if (_property != null)
				{
					return _property.GetValue(obj, null);
				}
				return _field.GetValue(obj);
			}

			public Type MemberType
			{
				get
				{
					if (_property != null)
					{
						return _property.PropertyType;
					}
					return _field.FieldType;
				}
			}

			public string Name
			{
				get
				{
					if (_property != null)
					{
						return _property.Name;
					}
					return _field.Name;
				}
			}
		}

		private class TypeMap
		{
			public readonly List<ValueMember> SingleSimple
				= new List<ValueMember>();

			public readonly Dictionary<ValueMember, Type> ManySimple
				= new Dictionary<ValueMember, Type>();
		}

		private static int _lineNumber = 1;

		public static T Parse<T>(string[,] input)
		{
			_lineNumber = 0;

			try
			{
				var type = typeof(T);

				// обрабатываем лист объектов
				if (typeof(T).IsGenericType)
				{
					Type genericArgument = type.GetGenericArguments()[0];

					IList list = (IList) typeof (List<>)
						.MakeGenericType(genericArgument)
						.GetConstructor(Type.EmptyTypes)
						.Invoke(null);

					for (int i = 1; i < input.GetLength(0); i++)
					{
						_lineNumber = i;
						var startWith = input[i, 0];
						if (String.IsNullOrEmpty(startWith) || startWith.StartsWith("$") || startWith.StartsWith("#"))
						{
							continue;
						}

						object obj = ParseOneLine(input, i, genericArgument);
						list.Add(obj);
					}

					return (T) list;
				}
				// обрабатываем один объект
				else
				{
					_lineNumber = 1;
					return (T) ParseOneLine(input, 1, type);
				}
			}
			catch (Exception e)
			{
				Error(e.Message);
				return default(T);
			}
		}

		private static object ParseOneLine(string[,] input, int line, Type type)
		{

			object obj = Activator.CreateInstance(type);
			TypeMap map = GetTypeMap(type);

			// назначаем все поля объекту
			for (int i = 0; i < input.GetLength(1); i++)
			{
				var name = input[0, i];
				bool finded = false;

				// поле является простым типом?
				foreach (var valMember in map.SingleSimple)
				{
					if (name == valMember.Name)
					{
						valMember.SetValue(ref obj,
							StringToObject(input[line, i], valMember.MemberType));
						finded = true;
						break;
					}
				}

				if (!finded)
				{
					// поле является массивом или листом?
					foreach (KeyValuePair<ValueMember, Type> valMember in map.ManySimple)
					{
						if (name == valMember.Key.Name)
						{
							IList list = (IList) typeof (List<>)
								.MakeGenericType(valMember.Value)
								.GetConstructor(Type.EmptyTypes)
								.Invoke(null);
							string[] elements = input[line, i].Split(',');

							foreach (var element in elements)
							{
								list.Add(StringToObject(element, valMember.Value));
							}

							if (valMember.Key.MemberType.IsArray)
							{
								Array y = Array.CreateInstance(valMember.Value, list.Count);
								list.CopyTo(y, 0);
								valMember.Key.SetValue(ref obj, y);
							}
							else
							{
								valMember.Key.SetValue(ref obj, list);
							}

							finded = true;
							break;
						}
					}
				}

				if (!finded)
				{
					Error("Field or Property " + name + " not found");
				}
			}
			return obj;
		}

		private static object StringToObject(string str, Type type)
		{
			if (type.IsEnum)
			{
				return Enum.Parse(type, str);
			}
			if (type == typeof (int))
			{
				return Convert.ToInt32(str, CultureInfo.InvariantCulture);
			}
			if (type == typeof (string))
			{
				str = str.Replace("\\\\", "\\");
				str = str.Replace("\\n", "\n");
				str = str.Replace("\\t", "\t");
				return str;
			}
			if (type == typeof (float))
			{
				return Convert.ToSingle(str, CultureInfo.InvariantCulture);
			}
			if (type == typeof (bool))
			{
				return str.ToUpperInvariant() == "TRUE";
			}

			Error("Not supported normalize for simple type " + type.Name);
			return str;
		}

		private static void Error(string msg)
		{
			Debug.LogError("Error on line" + _lineNumber + "\n" + msg);
		}

		private static void Warning(string msg)
		{
			Debug.LogWarning(msg);
		}
	}
}