﻿namespace Architecture.UnityModule.Serializer
{
	public static class Parser
	{
		public static string Serialize(object obj, bool format = false)
		{
			string str = JSON.Serialize(obj);
//			Debug.Log(JSON.Format(str));

			if (format)
			{
				str = JSON.Format(str);
			}

			return str;
		}

		public static T Deserialize<T>(string input)
		{
//			Debug.Log(JSON.Format(input));
			return JSON.Parse<T>(input);
		}
	}
}