﻿using System;
using Architecture.UnityModule.Extensions;
using UnityEngine;

namespace Architecture.UnityModule.BaseRoot
{
	public abstract class BaseA : MonoBehaviour
	{
		private void Awake()
		{
			_clearLinks.SafeCall();
			_clearLinks = Uninit;
			Init();
		}

		private static Action _clearLinks;

		protected abstract void Init();
		protected abstract void Uninit();
	}
}