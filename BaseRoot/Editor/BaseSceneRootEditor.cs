﻿using System;
using Architecture.UnityModule.BaseRoot;
using UnityEditor;
using UnityEngine;

namespace Architecture.Core.BaseRoot.Editor
{
	[CustomEditor(typeof(BaseA), true)]
	public class BaseSceneRootEditor : UnityEditor.Editor
	{
		private BaseA _editorItem;

		protected void OnEnable()
		{
			_editorItem = (BaseA) target;
		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			EditorGUILayout.HelpBox("Use the button below to ensure the context root " +
									"will be executed before any other injectable MonoBehaviour.", MessageType.Info);
			if (GUILayout.Button("Set execution order"))
			{
				var contextRootType = _editorItem.GetType();
				var contextRootOrder = SetScriptExecutionOrder(contextRootType, -5000, true);
				var message = string.Format("{0} execution order set to {1}.", contextRootType.Name, contextRootOrder);

				EditorUtility.DisplayDialog("Script execution order", message, "Ok");
			}
		}

		/// <summary>
		///     Sets the script execution order.
		/// </summary>
		/// <param name="type">Type to be set.</param>
		/// <param name="order">Order to be set.</param>
		/// <param name="unique">Indicates whether the execution order should be unique.</param>
		public static int SetScriptExecutionOrder(Type type, int order, bool unique)
		{
			var executionOrder = order;
			MonoScript selectedScript = null;

			// Get the first available execution order.
			var available = false;
			while (!available)
			{
				available = true;

				foreach (var script in MonoImporter.GetAllRuntimeMonoScripts())
				{
					if (selectedScript == null && script.GetClass() == type)
					{
						selectedScript = script;
						if (!unique)
						{
							break;
						}
					}

					if (script.GetClass() != type && MonoImporter.GetExecutionOrder(script) == executionOrder)
					{
						executionOrder += order;
						available = false;
					}
				}
			}

			MonoImporter.SetExecutionOrder(selectedScript, executionOrder);

			return executionOrder;
		}
	}
}