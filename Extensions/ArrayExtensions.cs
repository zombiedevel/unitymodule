﻿namespace Architecture.UnityModule.Extensions
{
	internal static class ArrayExtensions
	{
		public static bool IsValidIndex<T>(this T[,] array, int x)
		{
			if (x < 0)
			{
				return false;
			}

			if (array.GetLength(0) <= x)
			{
				return false;
			}

			return true;
		}

		public static bool IsValidIndex<T>(this T[,] array, int x, int y)
		{
			if (x < 0 || y < 0)
			{
				return false;
			}

			if (array.GetLength(0) <= x)
			{
				return false;
			}

			if (array.GetLength(1) <= y)
			{
				return false;
			}

			return true;
		}

		public static void Fill<T>(this T[] array, T val)
		{
			for (var x = 0; x < array.GetLength(0); x++)
			{
				array[x] = val;
			}
		}

		public static void Fill<T>(this T[,] array, T val)
		{
			for (var x = 0; x < array.GetLength(0); x++)
			{
				for (var y = 0; y < array.GetLength(1); y++)
				{
					array[x, y] = val;
				}
			}
		}
	}
}