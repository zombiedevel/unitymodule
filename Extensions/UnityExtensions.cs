using System.Collections.Generic;
using UnityEngine;

namespace Architecture.UnityModule.Extensions
{
	public static class UnityExtensions
	{
		public static T FindInParents<T>(this GameObject go)
		{
			if (go == null)
			{
				return default(T);
			}

			var comp = go.GetComponent<T>();

			if (comp == null)
			{
				var t = go.transform.parent;

				while (t != null && comp == null)
				{
					comp = t.gameObject.GetComponent<T>();
					t = t.parent;
				}
			}

			return comp;
		}

		public static T GetOrAddComponent<T>(this GameObject go) where T : Component
		{
			if (go == null)
			{
				return null;
			}

			var comp = go.GetComponent<T>();

			if (comp == null)
			{
				comp = go.AddComponent<T>();
			}

			return comp;
		}

		public static GameObject FindChildWithName(this GameObject go, string name)
		{
			if (go == null)
			{
				return null;
			}

			if (go.name == name)
			{
				return go;
			}

			foreach (var child in go.GetComponentsInChildren<Transform>(true))
			{
				if (child.name == name)
				{
					return child.gameObject;
				}
			}
			return null;
		}

		public static bool AddOnce<T>(this List<T> obj, T item)
		{
			if (obj.IndexOf(item) < 0)
			{
				obj.Add(item);
				return true;
			}
			return false;
		}

		public static bool RandomBool()
		{
			return Random.Range(0, 2) == 0;
		}

		public static T RandomItem<T>(this List<T> obj)
		{
			var c = obj.Count;
			if (c > 0)
			{
				return obj[Random.Range(0, c)];
			}
			Debug.LogError("List count == 0. Returned Default");
			return default(T);
		}

		public static T RandomItem<T>(this T[] obj)
		{
			var c = obj.Length;
			if (c > 0)
			{
				return obj[Random.Range(0, c)];
			}
			Debug.LogError("Array count == 0. Returned Default");
			return default(T);
		}

		public static string ToLongString(this Vector3 vector)
		{
			return vector.x + ":" + vector.y + ":" + vector.z;
		}

		public static bool FastNear(Vector3 a, Vector3 b, float radius)
		{
			if ((a.x - b.x).Abs() > radius )
			{
				return false;
			}

			if ((a.z - b.z).Abs() > radius )
			{
				return false;
			}

			return true;
		}
	}
}