﻿using System.Collections.Generic;
using Architecture.UnityModule.Pools;
using UnityEngine;

namespace Architecture.UnityModule.Extensions
{
	public static class CommonExtensions
	{
		public static GameObject PoolSpawn(this GameObject go)
		{
			return GameObjectPool.Instance.Get(go);
		}

		public static void PoolReturn(this GameObject go)
		{
			GameObjectPool.Instance.ToPool(go);
		}

		public static T PoolSpawn<T>(this T mb) where T : MonoBehaviour
		{
			return GameObjectPool.Instance.Get(mb.gameObject).GetComponent<T>();
		}

		public static void PoolReturn<T>(this T mb) where T : MonoBehaviour
		{
			GameObjectPool.Instance.ToPool(mb.gameObject);
		}

		public static T Last<T>(this List<T> list)
		{
			return list[list.Count - 1];
		}

		// если выпадает вероятность [0..1]
		public static bool CheckProbability(this float f)
		{
			return Random.value < f;
		}


		// возвращает 0, 1, 2 и тд в зависимости от вероятностей
		public static int ChoiseRandom(params float[] probs)
		{
			float sum = 0;
			foreach (var t in probs)
			{
				sum += t;
			}

			var randomPoint = Random.value * sum;
			for (var i = 0; i < probs.Length; i++)
			{
				if (randomPoint < probs[i])
				{
					return i;
				}
				randomPoint -= probs[i];
			}
			return probs.Length - 1;
		}

		public static Vector3 GetRandomDirInCone(this Transform t, float angleRadians)
		{
			return t.TransformDirection(Utils.Utils.GetRandomDirVectorInCone(angleRadians));
		}
	}
}