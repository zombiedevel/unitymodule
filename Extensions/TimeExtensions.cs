﻿using System;

namespace Architecture.UnityModule.Extensions
{
	public static class TimeExtensions
	{
		public static TimeSpan TimeElapsed(this DateTime date)
		{
			return DateTime.Now - date;
		}
	}
}