﻿using System;
using System.Collections.Generic;

namespace Architecture.UnityModule.Extensions
{
	public static class CollectionExtensions
	{
		private static readonly Random rnd = new Random();

		public static void Shuffle<T>(this IList<T> list)
		{
			var n = list.Count;
			while (n > 1)
			{
				n--;
				int k = rnd.Next(n + 1);
				var value = list[k];
				list[k] = list[n];
				list[n] = value;
			}
		}

		public static List<T> GetShuffleCopy<T>(this List<T> list)
		{
			var newList = new List<T>();
			foreach (var obj in list)
			{
				newList.Add(obj);
			}
			newList.Shuffle();
			return newList;
		}
	}
}