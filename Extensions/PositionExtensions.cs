﻿using UnityEngine;

namespace Architecture.UnityModule.Extensions
{
	public static class PositionExtensions
	{
		// Set
		public static void X(this Transform tr, float x)
		{
			var t = tr.position;
			t.x = x;
			tr.position = t;
		}

		public static void Y(this Transform tr, float y)
		{
			var t = tr.position;
			t.y = y;
			tr.position = t;
		}

		public static void Z(this Transform tr, float z)
		{
			var t = tr.position;
			t.z = z;
			tr.position = t;
		}

		public static void ShiftX(this Transform tr, float ox)
		{
			var t = tr.position;
			t.x += ox;
			tr.position = t;
		}

		public static void ShiftY(this Transform tr, float oy)
		{
			var t = tr.position;
			t.y += oy;
			tr.position = t;
		}

		public static void ShiftZ(this Transform tr, float oz)
		{
			var t = tr.position;
			t.z += oz;
			tr.position = t;
		}

		public static void LocX(this Transform tr, float x)
		{
			var t = tr.localPosition;
			t.x = x;
			tr.localPosition = t;
		}

		public static void LocY(this Transform tr, float y)
		{
			var t = tr.localPosition;
			t.y = y;
			tr.localPosition = t;
		}

		public static void LocZ(this Transform tr, float z)
		{
			var t = tr.localPosition;
			t.z = z;
			tr.localPosition = t;
		}

		// Get
		public static float X(this Transform tr)
		{
			return tr.position.x;
		}

		public static float Y(this Transform tr)
		{
			return tr.position.y;
		}

		public static float Z(this Transform tr)
		{
			return tr.position.z;
		}

		public static float LocX(this Transform tr)
		{
			return tr.localPosition.x;
		}

		public static float LocY(this Transform tr)
		{
			return tr.localPosition.y;
		}

		public static float LocZ(this Transform tr)
		{
			return tr.localPosition.z;
		}
	}
}