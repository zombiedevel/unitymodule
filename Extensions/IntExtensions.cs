﻿using System;
using System.Globalization;
using UnityEngine;

namespace Architecture.UnityModule.Extensions
{
	public static class IntExtensions
	{
		public static string ToStringInvariant(this int num)
		{
			return num.ToString(CultureInfo.InvariantCulture);
		}

		public static float ToFloat(this int num)
		{
			return Convert.ToSingle(num, CultureInfo.InvariantCulture);
		}

		public static int CyclicPrev(this int value, int min, int max)
		{
			if (--value < min)
			{
				value = max;
			}
			return value;
		}

		public static int CyclicNext(this int value, int min, int max)
		{
			if (++value > max)
			{
				value = min;
			}
			return value;
		}

		public static int Abs(this int num)
		{
			return Mathf.Abs(num);
		}
	}
}