﻿using System;
using UnityEngine;

namespace Architecture.UnityModule.Extensions
{
	public static class ActionExtensions
	{
		public static void SafeCall(this Action action)
		{
			try
			{
				if (action != null)
				{
					action();
				}
			}
			catch (Exception e)
			{
				Debug.LogError("Exception in callback: " + e);
			}
		}

		public static void SafeCall<T>(this Action<T> action, T param)
		{
			try
			{
				if (action != null)
				{
					action(param);
				}
			}
			catch (Exception e)
			{
				Debug.LogError("Exception in callback: " + e);
			}
		}

		public static void SafeCall<T, R>(this Action<T, R> action, T param1, R param2)
		{
			try
			{
				if (action != null)
				{
					action(param1, param2);
				}
			}
			catch (Exception e)
			{
				Debug.LogError("Exception in callback: " + e);
			}
		}

		public static void SafeCall<T1, T2, T3>(this Action<T1, T2, T3> action, T1 param1, T2 param2, T3 param3)
		{
			try
			{
				if (action != null)
				{
					action(param1, param2, param3);
				}
			}
			catch (Exception e)
			{
				Debug.LogError("Exception in callback: " + e);
			}
		}
	}
}