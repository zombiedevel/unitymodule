﻿using System;
using System.Globalization;
using UnityEngine;

namespace Architecture.UnityModule.Extensions
{
	public static class FloatExtensions
	{
		public static string ToStringInvariant(this float num)
		{
			return num.ToString(CultureInfo.InvariantCulture);
		}

		public static int ToInt(this float num)
		{
			return Convert.ToInt32(num);
		}

		public static bool IsEqualTolerance(this float num, float num2)
		{
			return Mathf.Abs(num - num2) < 0.001f;
		}

		public static float Abs(this float num)
		{
			return Mathf.Abs(num);
		}
	}
}