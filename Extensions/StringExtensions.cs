﻿using System;
using System.Globalization;

namespace Architecture.UnityModule.Extensions
{
	public static class StringExtensions
	{
		public static bool HasInfo(this string input)
		{
			return !string.IsNullOrEmpty(input);
		}

		public static int ToInt(this string str, int defaultValue = 0)
		{
			return Convert.ToInt32(str, CultureInfo.InvariantCulture);
		}

		public static float ToFloat(this string str, float defaultValue = 0)
		{
			return Convert.ToSingle(str, CultureInfo.InvariantCulture);
		}

		public static T ToEnum<T>(this string value)
		{
			return (T) Enum.Parse(typeof(T), value, true);
		}
	}
}