﻿using System;
using System.ComponentModel;
using UnityEngine;
using Random = System.Random;

namespace Architecture.UnityModule.Extensions
{
	public static class EnumExtension
	{
		public static string ToDescriptionString(this Enum val)
		{
			var attributes = (DescriptionAttribute[])
				val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
			return attributes.Length > 0 ? attributes[0].Description : string.Empty;
		}

		public static T Prev<T>(this T src) where T : struct
		{
			if (!typeof(T).IsEnum)
			{
				Debug.LogError(string.Format("Argumnent {0} is not an Enum", typeof(T).Name));
				return default(T);
			}

			var arr = (T[]) Enum.GetValues(src.GetType());
			var j = Array.IndexOf(arr, src) - 1;
			return j < 0 ? arr[arr.Length - 1] : arr[j];
		}

		public static T Next<T>(this T src) where T : struct
		{
			if (!typeof(T).IsEnum)
			{
				Debug.LogError(string.Format("Argumnent {0} is not an Enum", typeof(T).Name));
				return default(T);
			}

			var arr = (T[]) Enum.GetValues(src.GetType());
			var j = Array.IndexOf(arr, src) + 1;
			return j >= arr.Length ? arr[0] : arr[j];
		}
	}

	public static class Enum<T> where T : struct
	{
		public static T Parse(string value, bool ignoreCase = true)
		{
			try
			{
				return (T) Enum.Parse(typeof(T), value, ignoreCase);
			}
			catch
			{
				Debug.LogError("nor parse: " + value + ". Returned default");
				return default(T);
			}
		}

		public static T Random()
		{
			var v = Enum.GetValues(typeof(T));
			return (T) v.GetValue(new Random().Next(v.Length));
		}
	}
}