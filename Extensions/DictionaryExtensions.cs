﻿using System.Collections.Generic;

namespace Architecture.UnityModule.Extensions
{
	public static class DictionaryExtensions
	{
		public static TValue GetValueOrCreate<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, TValue defaultValue)
		{
			if (!dic.ContainsKey(key))
			{
				dic.Add(key, defaultValue);
			}
			return dic[key];
		}
	}
}