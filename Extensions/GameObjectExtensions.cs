﻿using UnityEngine;

namespace Architecture.UnityModule.Extensions
{
	public static class GameObjectExtensions
	{
		public static GameObject CreateSimularly(this GameObject newGo, GameObject asGo, bool setSourceAsParent = true)
		{
			if (setSourceAsParent)
			{
				return Object.Instantiate(newGo, asGo.transform.position, asGo.transform.rotation, asGo.transform);
			}
			else
			{
				return Object.Instantiate(newGo, asGo.transform.position, asGo.transform.rotation);
			}
		}
	}
}