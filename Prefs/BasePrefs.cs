﻿using System;

namespace Architecture.UnityModule.Prefs
{
	public abstract class BasePrefs
	{
		public string PrefsKey()
		{
			return GetType().Name;
		}

		public bool IsAutoSave()
		{
			foreach (object attribute in GetType().GetCustomAttributes(true))
			{
				if (attribute is AutosaveToPrefsAttribute)
				{
					return true;
				}
			}
			return false;
		}

		public bool IsObsolete()
		{
			foreach (object attribute in GetType().GetCustomAttributes(true))
			{
				if (attribute is ObsoleteAttribute)
				{
					return true;
				}
			}
			return false;
		}
	}
}