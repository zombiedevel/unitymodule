﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Architecture.UnityModule.Serializer;
using Architecture.UnityModule.Utils;
using UnityEngine;

namespace Architecture.UnityModule.Prefs
{
	public static class PrefsCore
	{
		public static readonly List<BasePrefs> AllPrefses = new List<BasePrefs>();
		private const string _PASSWORD = "I%%%L0V3-_-MRRRR_To_di3zzZ";
		private const string _CRYPTO_IDENTIFIER = "CRY-";

		public static T RegisterAndLoad<T>() where T : BasePrefs
		{
			T p = Load<T>();
			AllPrefses.Add(p);
			return p;
		}

		public static void SavePrefsMarkedAutoSave()
		{
			foreach (var basePrefs in AllPrefses)
			{
				if (basePrefs.IsAutoSave())
				{
					basePrefs.Save();
				}
			}
			PlayerPrefs.Save();
		}


		private static T Load<T>()
		{
			var obj = Activator.CreateInstance<T>();
			var basePrefs = obj as BasePrefs;
			if (basePrefs == null)
			{
				Debug.LogError("obj is not BasePrefs");
				return obj;
			}
			var key = basePrefs.PrefsKey();
			if (HasKey(key))
			{
				var text = GetString(key);
				var stub = Parser.Deserialize<T>(text);
				if (stub != null)
				{
					obj = stub;
				}
				else
				{
					Debug.LogError("Prefs data not parsed: " + key + ". Load default.");
				}
			}
			else
			{
				basePrefs.Save();
			}
			return obj;
		}

		public static void Save(this BasePrefs prefs)
		{
//			Debug.Log("Save" + prefs.PrefsKey());
			var data = Parser.Serialize(prefs);
			if (data != null)
			{
				SetString(prefs.PrefsKey(), data);
			}
			else
			{
				Debug.LogError("Serialize prefs data is null: " + prefs.PrefsKey());
			}
		}

		private static void SetString(string key, string value)
		{
			var hashedKey = GenerateMD5(key);
			var encryptedValue = CustomDES.Encrypt(value, _PASSWORD);
			PlayerPrefs.SetString(_CRYPTO_IDENTIFIER + hashedKey, encryptedValue);
		}

		private static string GetString(string key)
		{
			var hashedKey = GenerateMD5(key);
			var encryptedValue = PlayerPrefs.GetString(_CRYPTO_IDENTIFIER + hashedKey);
			return CustomDES.Decrypt(encryptedValue, _PASSWORD);
		}

		private static bool HasKey(string key)
		{
			var hashedKey = GenerateMD5(key);
			var hasKey = PlayerPrefs.HasKey(_CRYPTO_IDENTIFIER + hashedKey);
			return hasKey;
		}

		private static string GenerateMD5(string text)
		{
			var md5 = MD5.Create();
			var inputBytes = Encoding.UTF8.GetBytes(text);
			var hash = md5.ComputeHash(inputBytes);

			var sb = new StringBuilder();
			foreach (var t in hash)
			{
				sb.Append(t.ToString("X2"));
			}
			return sb.ToString();
		}
	}
}